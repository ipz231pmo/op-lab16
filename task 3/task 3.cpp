﻿#include <stdio.h>
int reverseNumber(int num) {
    int n1 = 0;
    while (num > 0) {
        n1 = n1 * 10 + num % 10;
        num /= 10;
    }
    return n1;
}
int main(){
    printf("Enter a number: ");
    int num;
    scanf_s("%d", &num);
    printf("Reversed number is %d\n", reverseNumber(num));
    return 0;
}