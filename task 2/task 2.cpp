﻿#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define ARRAY_SIZE 10

int sumArray(int array[]) {
    int res = 0;
    for (int i = 0; i < ARRAY_SIZE; i++) res += array[i];
    return res;
}
int productArray(int array[]) {
    int product = 1;
    for (int i = 0; i < ARRAY_SIZE; i++) product *= array[i];
    return product;
}
int maxArray(int array[]) {
    int max = array[0];
    for (int i = 1; i < ARRAY_SIZE; i++)
        if (array[i] > max) max = array[i];
    return max;
}
int minArray(int array[]) {
    int min = array[0];
    for (int i = 1; i < ARRAY_SIZE; i++)
        if (array[i] < min) min = array[i];
    return min;
}
void randomiseArray(int array[]) {
    for (int i = 0; i < ARRAY_SIZE; i++) array[i] = rand() % 201 - 100;
}
void printArray(int array[]) {
    printf("array = { ");
    for (int i = 0; i < ARRAY_SIZE; i++) printf("%d ", array[i]);
    printf(" }\n");
}
int main(){
    int arr[ARRAY_SIZE];
    randomiseArray(arr);
    printArray(arr);
    printf("Sum of array elements is %d\n", sumArray(arr));
    printf("Product of array elements is %d\n", productArray(arr));
    printf("Minimun element of array is %d\n", minArray(arr));
    printf("Maximum element of array is %d\n", maxArray(arr));
    return 0;
}
